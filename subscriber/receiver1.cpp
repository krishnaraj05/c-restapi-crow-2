#include "receiver1.h"
#include <iostream>
#include <sstream>
#include <proton/message.hpp>
#include <nlohmann/json.hpp>
#include <crow/middlewares/cors.h>

SimpleReceive::SimpleReceive() : dbconnect("dbname=mydb user=root password=root host=127.0.0.1 port=5432"){

}


void SimpleReceive::on_message(proton::delivery &d, proton::message &msg) {
    std::cout << "Received message : " << msg.body() << std::endl;
    std::stringstream msgBodyStream;
    msgBodyStream << msg.body();
    std::string msgBodyStr = msgBodyStream.str();

    pqxx::work dbwork(dbconnect);
    std::string insertquery = "INSERT INTO numbers (no) VALUES ('" + msgBodyStr + "')";
    dbwork.exec(insertquery);
    dbwork.commit();

}


nlohmann::json MyApp::handleRequest() {
    std::cout << "Success" << std::endl;
    pqxx::work txn(dbconnect);
    pqxx::result fetchquery = txn.exec("SELECT no FROM numbers");
        nlohmann::json response;

        for (const auto& row : fetchquery) {
            nlohmann::json student;
            student["Number"] = row["no"].as<std::string>();
            response.push_back(student);
        }
        // txn.exec("DELETE FROM numbers");
        txn.commit();
        return response;
}

MyApp::MyApp() : app(), dbconnect("dbname=mydb user=root password=root host=127.0.0.1 port=5432") {

        CROW_ROUTE(app, "/hello").methods(crow::HTTPMethod::GET)
        ([this](const crow::request& req) {
            crow::response response(nlohmann::json{
                this->handleRequest()
            }.dump());
        response.add_header("Access-Control-Allow-Origin", "*");

        return response;
    });
        
}


void MyApp::run(){
    app.port(8080).multithreaded().run();
}
