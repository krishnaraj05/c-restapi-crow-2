#include <proton/container.hpp>
#include <proton/messaging_handler.hpp>
#include <pqxx/pqxx>
#include <nlohmann/json.hpp>
#include <crow.h>


using namespace std;

class SimpleReceive : public proton::messaging_handler {
// private:
//     string address;
//     string topic;
//     proton::receiver receiver;
    pqxx::connection dbconnect;
public:
    SimpleReceive();
    // void on_container_start(proton::container &c) override; 
    void on_message(proton::delivery &d, proton::message &msg) override; 
    
};

class MyApp {
private:
    crow::SimpleApp app;
    pqxx::connection dbconnect;
    nlohmann::json handleRequest();

public:
    MyApp();
    void run();
};
