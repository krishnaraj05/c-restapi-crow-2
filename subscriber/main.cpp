#include <proton/container.hpp>
#include <iostream>
#include "receiver1.h"
#include <thread>
#include <proton/connection_options.hpp>

using namespace std;

int main() {
    MyApp myApp;

    auto runAMQP = []() {
        const std::string username = "admin";
        const std::string password = "admin";

        const std::string connection_url = "amqp://127.0.0.1:5672";
        const std::string topic_address = "amqp://127.0.0.1:5672/topic/RCS.E2K.TMS.Dummy";

        SimpleReceive receiver_handler;
        proton::container container(receiver_handler);

        proton::connection connection = container.connect(connection_url, proton::connection_options().user(username).password(password));
        proton::session session = connection.open_session();

        proton::receiver receiver = session.open_receiver(topic_address);

        container.run();
    };

    std::thread appThread([&myApp]() {
        myApp.run();
    });

    std::thread amqpThread(runAMQP);

    appThread.join();
    amqpThread.join();

    return 0;
}
