#include <proton/container.hpp>
#include <thread>
#include <iostream>
#include "sender1.h"
#include <proton/connection_options.hpp>

using namespace std;

int main() {
    // string address("amqp:tcp://127.0.0.1:5672/mybroker");
    // string topic("jms.topic.tasksecond");
    // continuesender sender(address, topic);

    // proton::container container(sender);

    // thread container_thread([&]() { container.run(); });
    // thread sender_thread([&]() { sender.send_message(); });
    
    // sender_thread.join();
    // container_thread.join();

    const std::string username = "admin";
    const std::string password = "admin";

    const std::string connection_url = "amqp://127.0.0.1:5672/";
    continuesender sender_handler;

    proton::container container(sender_handler);

    proton::connection connection = container.connect(connection_url, proton::connection_options().user(username).password(password));

    proton::session session = connection.open_session();

    proton::sender sender = session.open_sender("amqp://127.0.0.1:5672/topic/RCS.E2K.TMS.Dummy");

    container.run();
    
    return 0;
}

