#include <proton/url.hpp>
#include <proton/message.hpp>
#include <proton/sender.hpp>
#include <iostream>
#include "sender1.h"
#include <thread>
#include <cstdlib>
#include <chrono>
#include <ctime>
#include <mutex>

// continuesender::continuesender(string &addr , string &top) : address(addr), topic(top) {}

// void continuesender::on_container_start(proton::container &c) {
//     proton::url url(address);
//     c.connect(url);
//     sender = c.open_sender(address);
// }

// void continuesender::send_message() {

//     srand(time(0));
    
//      while (true) {
//         int randomnumber = (rand() % 30) + 70;


//         cout << "Random number: " << randomnumber << endl;
        
//         this_thread::sleep_for(chrono::seconds(5));
//         proton::message msg;
//         msg.body(randomnumber);
//         msg.to(topic);
//         sender.send(msg);
        
        
//     }
// }


//  char choice;
//     do {
//         string body;
//         time_t india = time(0);
//         char*dt = ctime(&india);
//         cout << "To give The Message type y/n :- ";
//         cin >> choice;
//         if (choice == 'n') {
//             break;
//         }
//         if (choice == 'y') {
//             proton::message msg;
//             cout << "Enter the message: ";
//             cin.ignore();
//             getline(cin, body);
//             msg.body(body);
//             msg.properties().put("timestamp", string(dt));
//             msg.to(topic);
//             sender.send(msg);
//         } else {
//             cout << "Invalid. enter 'y' or 'n'." <<::endl;
//         }
//     } while (choice == 'y');

void continuesender::on_sendable(proton::sender& s) {

    srand(time(0));

    if(s.credit() > 0) {

        int randomnumber = (rand() % 30) + 70;

        std::cout << "Random number: " << randomnumber << endl;

        this_thread::sleep_for(chrono::seconds(2));
        

        const std::string topic_address = "amqp://127.0.0.1:5672/topic/RCS.E2K.TMS.Dummy";

        
        proton::message message;
        message.body(randomnumber);
        message.address(topic_address);
        s.send(message);

    }

}
